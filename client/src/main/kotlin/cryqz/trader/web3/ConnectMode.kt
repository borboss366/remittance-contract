package cryqz.trader.web3

enum class ConnectMode {
    IPC, HTTP;
}
