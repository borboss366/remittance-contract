package cryqz.trader.web3

import org.web3j.protocol.Web3jService
import org.web3j.protocol.http.HttpService
import org.web3j.protocol.ipc.UnixIpcService

data class ConnectInfo(val url: String, val mode: ConnectMode = ConnectMode.HTTP, val description: String = "") {
    fun createService(): Web3jService {
        return when(mode) {
            ConnectMode.IPC -> UnixIpcService(url)
            ConnectMode.HTTP -> HttpService(url)
        }
    }

    companion object {
        fun buildFromAddress(address: String, port: Int) = ConnectInfo("$address:$port")
        fun buildLocalhostPlain(): ConnectInfo = buildFromAddress("http://127.0.0.1", 8545)
    }
}