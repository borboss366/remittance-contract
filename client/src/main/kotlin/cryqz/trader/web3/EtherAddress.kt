package cryqz.trader.web3

data class EtherAddress (val pblcKy: String) {
    override fun toString() = if (zeroAddress) "Zero address" else pblcKy
    val zeroAddress = pblcKy == "0x0000000000000000000000000000000000000000"

    companion object {
        fun createPrefixed(key: String): EtherAddress {
            return if (key.startsWith("0x")) EtherAddress(key) else EtherAddress("0x$key")
        }
        fun createZeroAddress() = EtherAddress("0x0000000000000000000000000000000000000000")
    }
}