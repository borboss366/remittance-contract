package cryqz.trader.web3

data class NetworkId(val id: Int, val name: String) {
    companion object {
        fun createFrom(str: String): NetworkId {
            return try {
                createFromId(Integer.parseInt(str))
            } catch(e: Exception) {
                NetworkId(666, str)
            }
        }

        fun createFromId(id: Int): NetworkId {
            return when(id) {
                1 -> NetworkId(id, "Mainnet")
                2 -> NetworkId(id, "Morden")
                3 -> NetworkId(id, "Ropsten")
                4 -> NetworkId(id, "Rinkeby")
                else -> NetworkId(id, "Test")
            }
        }
    }
}