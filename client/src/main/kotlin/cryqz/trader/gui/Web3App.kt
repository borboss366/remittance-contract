package cryqz.trader.gui

import cryqz.trader.gui.accounts.SelectClickAccountFragment
import cryqz.trader.gui.accounts.SelectedAccountFragment
import cryqz.trader.gui.accounts.UnlockAccountFragment
import cryqz.trader.gui.connect.ConnectFragment
import cryqz.trader.gui.web3.Web3StatusFragment
import javafx.application.Application
import javafx.scene.control.TabPane
import tornadofx.*

class AccountSelectView: View() {
    override val root = vbox {
        add(find<SelectClickAccountFragment>())
        add(find<SelectedAccountFragment>())
        add(find<UnlockAccountFragment>())
        minWidth = 800.0
    }
}

class ConnectView: View() {
    override val root = vbox {
        add(find<ConnectFragment>())
        add(find<Web3StatusFragment>())
        minWidth = 800.0
    }
}

class Web3View: View() {
    private val accountView: AccountSelectView by inject()
    private val connectView: ConnectView by inject()

    override val root = tabpane {
        tab("Connect") { add(connectView) }
        tab("Account") { add(accountView) }
        tab("Contract") { }
        tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE
    }
}

class Web3App : App(Web3View::class, Styles::class)

fun main(args: Array<String>) {
    Application.launch(Web3App::class.java, *args)
}