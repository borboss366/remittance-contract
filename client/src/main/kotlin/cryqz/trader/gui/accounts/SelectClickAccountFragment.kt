package cryqz.trader.gui.accounts

import cryqz.trader.accounts.Account
import cryqz.trader.gui.controller.AccountsController
import javafx.collections.FXCollections
import rx.javafx.kt.observeOnFx
import rx.javafx.kt.onChangedObservable
import rx.lang.kotlin.subscribeBy
import rx.lang.kotlin.toObservable
import tornadofx.*

class SelectClickAccountFragment: Fragment() {
    private val controller: AccountsController by inject()
    private val accountsList = FXCollections.observableArrayList<Account>()

    init {
        controller.accountModel.controlAccounts().observeOnFx().subscribeBy(onNext = { accountsList.setAll(it) })
    }

    override val root = form{
        fieldset("Accounts") {
            tableview(accountsList) {
                readonlyColumn("Name", Account::name)
                readonlyColumn("Type", Account::type)
                readonlyColumn("Address", Account::address)

                selectionModel.selectedItemProperty().onChange {
                    if (it != null) controller.web3Model.setAccount(it)
                }
            }
        }
    }
}