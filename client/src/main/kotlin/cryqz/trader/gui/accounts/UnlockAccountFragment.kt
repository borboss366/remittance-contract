package cryqz.trader.gui.accounts

import cryqz.trader.gui.controller.AccountsController
import tornadofx.*

class UnlockAccountFragment: Fragment() {
    private val controller: AccountsController by inject()
    private val model = PasswordModel()

    override val root = form {
        fieldset("Unlock account") {
            field("Password") { textfield(model.itemProperty) }
            button("Unlock") { setOnAction { unlock() } }
        }
    }

    private fun unlock() {
        if (model.commit()) {
            controller.web3Model.unlockAccount(model.item)
        }
    }

    private class PasswordModel : ItemViewModel<String>()
}