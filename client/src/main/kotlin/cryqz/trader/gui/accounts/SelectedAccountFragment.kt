package cryqz.trader.gui.accounts

import cryqz.trader.accounts.Account
import cryqz.trader.gui.controller.AccountsController
import cryqz.trader.web3.EtherAddress
import cryqz.util.Either
import cryqz.util.initialized
import cryqz.util.makeStringFromWhatIsInside
import rx.javafx.kt.observeOnFx
import rx.lang.kotlin.subscribeBy
import tornadofx.*

/**
 * Information about the selected account
 */

fun Either<String, Account>.name() = bind { Either.ret<String, String>(it.name) }.makeStringFromWhatIsInside()
fun Either<String, Account>.type() = bind { Either.ret<String, String>(it.type) }.makeStringFromWhatIsInside()
fun Either<String, Account>.address() = bind { Either.ret<String, EtherAddress>(it.address) }.makeStringFromWhatIsInside()

class SelectedAccountFragment: Fragment() {
    private val controller: AccountsController by inject()
    private val model = SelectedAccountModel()

    private class SelectedAccountModel: ItemViewModel<Either<String, Account>>(Either.left("Not initialized")) {
        val initialized = bind(Either<String, Account>::initialized)
        val name = bind(Either<String, Account>::name)
        val type = bind(Either<String, Account>::type)
        val address = bind(Either<String, Account>::address)
    }

    init {
        controller.web3Model.unlockedAccountO().observeOnFx().subscribeBy(onNext = { model.item = it })
    }

    override val root = form {
        fieldset("Selected account") {
            field { label(model.initialized) }
            field("Name") { label(model.name)  }
            field("Type") { label(model.type)  }
            field("Address") { label(model.address) }
        }
    }
}