package cryqz.trader.gui

import cryqz.util.Either
import cryqz.util.filterLeft
import cryqz.util.filterRight
import rx.Observable
import rx.javafx.kt.observeOnFx

fun <L: Any, R: Any> Observable<Either<L, R>>.rightFX() = filterRight().observeOnFx()
fun <L: Any, R: Any> Observable<Either<L, R>>.leftFx() = filterLeft().observeOnFx()