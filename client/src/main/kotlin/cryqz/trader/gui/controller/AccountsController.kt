package cryqz.trader.gui.controller

import cryqz.trader.models.accounts.AccountsModelStub
import cryqz.trader.models.connects.ConnectModelStub
import cryqz.trader.models.web3.Web3Model
import tornadofx.Controller

class AccountsController : Controller() {
    val web3Model: Web3Model = Web3Model.Web3MultiModel()
    val accountModel = AccountsModelStub.fromFile("accounts")
    val connectModel = ConnectModelStub()
}