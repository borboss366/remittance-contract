package cryqz.trader.gui.web3

import cryqz.trader.gui.controller.AccountsController
import cryqz.trader.models.web3.Environment
import cryqz.trader.models.web3.Web3NetworkId
import cryqz.trader.web3.NetworkId
import cryqz.util.Either
import cryqz.util.initialized
import cryqz.util.makeStringFromWhatIsInside
import rx.javafx.kt.observeOnFx
import rx.lang.kotlin.subscribeBy
import tornadofx.*

fun Either<String, Web3NetworkId>.networkId() = bind { Either.ret<String, NetworkId>(it.id) }.makeStringFromWhatIsInside()

class Web3StatusFragment: Fragment() {
    private val controller: AccountsController by inject()
    private val model = Web3StatusModel()

    private class Web3StatusModel: ItemViewModel<Either<String, Web3NetworkId>>(Either.left("Not initialized")) {
        val initialized = bind(Either<String, Environment>::initialized)
        val networkId = bind(Either<String, Web3NetworkId>::networkId)
    }

    init {
        controller.web3Model.web3NetworkId().observeOnFx().subscribeBy(onNext = { model.item = it })
    }

    override val root = form {
        fieldset("Web3") {
            field { label(model.initialized) }
            field("ID") { label(model.networkId) }
        }
    }
}