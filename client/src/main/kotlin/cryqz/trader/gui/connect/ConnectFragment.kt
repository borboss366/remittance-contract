package cryqz.trader.gui.connect

import cryqz.trader.gui.controller.AccountsController
import cryqz.trader.web3.*
import cryqz.util.Either
import cryqz.util.fromNull
import cryqz.util.initialized
import cryqz.util.toDefault
import javafx.collections.FXCollections
import rx.javafx.kt.observeOnFx
import rx.lang.kotlin.subscribeBy
import tornadofx.*

class ConnectFragment: Fragment() {
    private val controller: AccountsController by inject()
    private val connectsList = FXCollections.observableArrayList<ConnectInfo>()
    private val model = SelectedConnectModel()

    private class SelectedConnectModel: ItemViewModel<ConnectInfo>(ConnectInfo.buildLocalhostPlain()) {
        val url = bind { item?.url?.toProperty() }
        val mode = bind { item?.mode?.toProperty() }
        val description = bind { item?.description?.toProperty() }

        override fun onCommit() {
            item = item.copy(url = url.value, mode = mode.value, description = description.value)
        }
    }

    init {
        controller.connectModel.connects().observeOnFx().subscribeBy(onNext = { connectsList.setAll(it) })
    }

    private fun connect() {
        if (model.commit()) {
            controller.web3Model.connect(model.item)
        }
    }


    override val root = form {
        fieldset("Available connects") {
            tableview(connectsList) {
                readonlyColumn("Description", ConnectInfo::description)
                readonlyColumn("URL", ConnectInfo::url)
                readonlyColumn("Mode", ConnectInfo::mode)
                selectionModel.selectedItemProperty().onChange {
                    model.item = it ?: ConnectInfo.buildLocalhostPlain()
                }
            }
        }
        fieldset("Connect info") {
            field("URL") { textfield(model.url) }
            field("Mode") {
                choicebox(model.mode, ConnectMode.values().toList()) {
                    selectionModel.select(ConnectMode.HTTP)
                }
            }
            field("Description") { textfield(model.description) }
            buttonbar {
                button("Connect") { setOnAction { connect() } }
            }
        }
    }
}