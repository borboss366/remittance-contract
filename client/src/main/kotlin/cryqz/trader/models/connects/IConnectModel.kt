package cryqz.trader.models.connects

import cryqz.trader.web3.ConnectInfo
import rx.Observable

interface IConnectModel {
    fun connects(): Observable<List<ConnectInfo>>
}