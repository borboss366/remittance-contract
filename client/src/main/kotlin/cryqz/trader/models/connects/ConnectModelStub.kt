package cryqz.trader.models.connects

import cryqz.trader.web3.ConnectInfo
import cryqz.trader.web3.ConnectMode
import rx.Observable

class ConnectModelStub: IConnectModel {
    private val connectsInfoO: Observable<List<ConnectInfo>> = Observable.just(listOf(
            ConnectInfo("http://127.0.0.1:8545", ConnectMode.HTTP, "ganache"),
            ConnectInfo("https://92.61.67.11:8545", ConnectMode.HTTP, "rinkeby-cryqz-outer"),
            ConnectInfo("https://192.168.0.239:8545", ConnectMode.HTTP, "rinkeby-cryqz"),
            ConnectInfo("/usr/ethereum/geth.ipc", ConnectMode.IPC, "rinkeby-cryqz")
    ))
    override fun connects() = connectsInfoO
}