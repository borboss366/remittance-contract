package cryqz.trader.models.web3

import cryqz.trader.web3.NetworkId
import org.web3j.protocol.Web3j

data class Web3NetworkId(val web3: Web3j, val id: NetworkId)