package cryqz.trader.models.web3

import cryqz.trader.accounts.Account
import cryqz.trader.web3.NetworkId
import cryqz.trader.web3.ConnectInfo
import cryqz.util.Either
import cryqz.util.flatBindSingle
import cryqz.util.fmap
import cryqz.util.zipWithPair
import org.web3j.protocol.Web3j
import org.web3j.tx.ReadonlyTransactionManager
import org.web3j.tx.TransactionManager
import rx.Observable
import java.math.BigInteger
import java.util.concurrent.ScheduledExecutorService

interface IWeb3Model {
    fun connect(ci: ConnectInfo)
    fun setCustomGasPrice(gp: BigInteger)

    fun connectInfoO(): Observable<Either<String, ConnectInfo>>
    fun pollingIntervalO(): Observable<Long>
    fun serviceO(): Observable<ScheduledExecutorService>
    fun customGasPrice0(): Observable<BigInteger>
    fun networkGasPriceO(): Observable<Either<String, BigInteger>> {
        return web3jO().flatBindSingle { it.ethGasPrice().observable() }.fmap { it.gasPrice }
    }

    fun gasPriceO(): Observable<Either<String, BigInteger>> {
        return networkGasPriceO().mergeWith(customGasPrice0().withLatestFrom(networkGasPriceO()) {
            cgp, ngp -> ngp.seq(Either.ret(cgp))
        })
    }

    fun readOnlyTransactionManagerO(): Observable<Either<String, TransactionManager>> {
        return web3jO().fmap { ReadonlyTransactionManager(it, Account.ReadOnly().address.pblcKy) }
    }

    fun web3jO(): Observable<Either<String, Web3j>>
    fun networkVersionO(): Observable<Either<String, NetworkId>> {
        return web3jO().flatBindSingle { it.netVersion().observable().single() }.fmap { NetworkId.createFrom(it.netVersion) }
    }

    fun web3NetworkId(): Observable<Either<String, Web3NetworkId>> {
        return web3jO().zipWithPair(networkVersionO()).fmap { Web3NetworkId(it.first, it.second) }
    }

    fun setAccount(account: Account)
    fun unlockedAccountO(): Observable<Either<String, Account>>
    //fun unlockedAccountBalance(): Observable<Either<String, BigInteger>>

    // unlocks the account, it is was loaded from wallet
    fun unlockAccount(password: String)
    
    fun transactionManagerO(): Observable<Either<String, TransactionManager>>

    fun readOnlyEnvironment(): Observable<Either<String, Environment>> {
        return Environment.buildFromObservables<String>(
                Observable.just(Either.ret<String, Account>(Account.ReadOnly())),
                readOnlyTransactionManagerO(),
                web3jO(),
                Observable.just(Either.ret((BigInteger.ZERO))))
    }

    fun functionalEnvironment(): Observable<Either<String, Environment>> {
        return Environment.buildFromObservables(
                unlockedAccountO(),
                transactionManagerO(),
                web3jO(),
                gasPriceO())
    }
}


