package cryqz.trader.models.web3

import cryqz.trader.accounts.Account
import cryqz.trader.accounts.createTransactionManager
import cryqz.trader.web3.ConnectInfo
import cryqz.util.*
import org.web3j.protocol.Web3j
import org.web3j.tx.TransactionManager
import org.web3j.utils.Async
import rx.Observable
import rx.schedulers.Schedulers
import rx.subjects.BehaviorSubject
import java.math.BigInteger
import java.util.concurrent.ScheduledExecutorService

sealed class Web3Model: IWeb3Model {
    private val pollingIntervalSubject: BehaviorSubject<Long> =
            BehaviorSubject.create(15_000L)
    private val pollingIntervalO = pollingIntervalSubject.asObservable()
    override fun pollingIntervalO(): Observable<Long> = pollingIntervalO

    private val serviceSubject: BehaviorSubject<ScheduledExecutorService> =
            BehaviorSubject.create(Async.defaultExecutorService())
    private val serviceO = serviceSubject.asObservable()
    override fun serviceO(): Observable<ScheduledExecutorService> = serviceO

    private val connectInfoSubject: BehaviorSubject<Either<String, ConnectInfo>> =
            BehaviorSubject.create(Either.left("Connect info not specified"))
    private val connectInfoO = connectInfoSubject.asObservable()
    override fun connectInfoO(): Observable<Either<String, ConnectInfo>> = connectInfoO

    private val customGasPriceSubject: BehaviorSubject<BigInteger> = BehaviorSubject.create()
    private val customGasPriceO = customGasPriceSubject.asObservable()
    override fun customGasPrice0(): Observable<BigInteger> = customGasPriceO

    private val accountSubject: BehaviorSubject<Either<String, Account>> =
            BehaviorSubject.create(Either.left("Account not initialized"))
    private val accountO = accountSubject.asObservable().distinct()
    fun accountO(): Observable<Either<String, Account>> = accountO

    private val passwordSubject: BehaviorSubject<String> = BehaviorSubject.create()
    private val passwordO = passwordSubject.asObservable()
    fun passwordO(): Observable<String> = passwordO

    override fun connect(ci: ConnectInfo) = connectInfoSubject.onNext(Either.ret(ci))
    override fun setCustomGasPrice(gp: BigInteger) = customGasPriceSubject.onNext(gp)
    override fun setAccount(account: Account) = accountSubject.onNext(Either.ret(account))
    override fun unlockAccount(password: String) = passwordSubject.onNext(password)
    override fun transactionManagerO(): Observable<Either<String, TransactionManager>> {
        return unlockedAccountO().zipWithPair(web3jO()).fmap { (acc, w3) -> acc.createTransactionManager(w3) }
    }


    class Web3SingleModel: Web3Model() {
        override fun web3jO(): Observable<Either<String, Web3j>> {
            return connectInfoO().buildWeb3j(pollingIntervalO(), serviceO()).share()
        }

        private val passwordUnlockedO = passwordO().withLatestFrom(accountO()) {
            pe, ae -> ae bind { a -> Either.ret<String, Account>(a.try2Unlock(pe)) }
        }.filterPred { it is Account.PrivateAccount }

        private val unlockedAccountO = accountO().mergeWith(passwordUnlockedO)
        override fun unlockedAccountO(): Observable<Either<String, Account>> = unlockedAccountO
//        private val accountBalanceO: Observable<Either<String, BigInteger>> =
//                unlockedAccountO.map { Either.Left<String, BigInteger>("Loading") }
//                        .mergeWith()
    }

    class Web3MultiModel: Web3Model() {
        override fun web3jO(): Observable<Either<String, Web3j>> {
            return connectInfoO().observeOn(Schedulers.io()).buildWeb3j(pollingIntervalO(), serviceO()).share()
        }

        private val passwordUnlockedO = passwordO().observeOn(Schedulers.io()).withLatestFrom(accountO().observeOn(Schedulers.io())) {
            pe, ae -> ae bind { a -> Either.ret<String, Account>(a.try2Unlock(pe)) }
        }.filterPred { it is Account.PrivateAccount }

        private val unlockedAccountO = accountO().mergeWith(passwordUnlockedO)
        override fun unlockedAccountO(): Observable<Either<String, Account>> = unlockedAccountO
    }
}

