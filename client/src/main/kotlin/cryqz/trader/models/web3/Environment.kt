package cryqz.trader.models.web3

import cryqz.trader.accounts.Account
import cryqz.util.Either
import cryqz.util.bind2Pair
import cryqz.util.fmap
import cryqz.util.zipWithPair
import org.web3j.protocol.Web3j
import org.web3j.tx.TransactionManager
import rx.Observable
import java.math.BigInteger

data class Environment(val account: Account, val manager: TransactionManager, val web3: Web3j, val gasPrice: BigInteger) {
    companion object {
        fun <L: Any> buildFromObservables(
                accO: Observable<Either<L, Account>>,
                managerO: Observable<Either<L, TransactionManager>>,
                web3O: Observable<Either<L, Web3j>>,
                gasPriceO: Observable<Either<L, BigInteger>>): Observable<Either<L, Environment>> {

            return Observable.combineLatest(accO, managerO, web3O, gasPriceO) {
                acce, manae, webe, gpe ->
                acce.bind2Pair(manae).bind2Pair(webe).bind2Pair(gpe) bind {
                    Either.ret<L, Environment>(Environment(it.first.first.first, it.first.first.second, it.first.second, it.second))
                }
            }
        }
    }
}