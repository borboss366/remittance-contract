package cryqz.trader.models.web3

import cryqz.trader.web3.ConnectInfo
import cryqz.util.Either
import org.web3j.protocol.Web3j
import rx.Observable
import java.util.concurrent.ScheduledExecutorService

fun Observable<Either<String, ConnectInfo>>.buildWeb3j(
        pollingO: Observable<Long>,
        serviceO: Observable<ScheduledExecutorService>): Observable<Either<String, Web3j>> {
    return  withLatestFrom(pollingO, serviceO) {
        ci, pi, s -> ci bind { Either.ret<String, Web3j>(Web3j.build(it.createService(), pi, s)) }
    }
}

