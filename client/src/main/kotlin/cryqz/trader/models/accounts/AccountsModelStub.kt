package cryqz.trader.models.accounts

import cryqz.trader.accounts.Account
import cryqz.util.Either
import rx.Observable
import java.io.File

class AccountsModelStub(accountsFolder: File): IAccountsModel {
    private val accountsO: Observable<List<Account>>
    init {
        val accountList = accountsFolder
                .listFiles()
                .filter { it.isFile }
                .map { Account.loadCryptoEA(it) }
                .flatMap { if (it is Either.Right) listOf(it.r) else emptyList() }
        accountsO = Observable.just(accountList)
    }

    override fun controlAccounts(): Observable<List<Account>> = accountsO
    override fun usedAccounts(): Observable<List<Account>> = Observable.empty()
    override fun addUsedAccount(account: Account) {
    }

    companion object {
        fun fromFile(name: String) = AccountsModelStub(File(name))
    }
}