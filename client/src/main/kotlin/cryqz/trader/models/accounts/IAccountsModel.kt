package cryqz.trader.models.accounts

import cryqz.trader.accounts.Account
import rx.Observable

interface IAccountsModel {
    fun controlAccounts(): Observable<List<Account>>
    fun usedAccounts(): Observable<List<Account>>
    fun addUsedAccount(account: Account)
}