package cryqz.trader.accounts

import cryqz.util.Either
import cryqz.util.fmap
import org.web3j.protocol.Web3j
import org.web3j.tx.ClientTransactionManager
import org.web3j.tx.FastRawTransactionManager
import org.web3j.tx.ReadonlyTransactionManager
import org.web3j.tx.TransactionManager
import org.web3j.utils.Convert
import rx.Observable
import java.math.BigDecimal
import java.math.BigInteger

fun Account.createTransactionManager(web3j: Web3j): TransactionManager {
    return when(this) {
        is Account.ReadOnly -> ReadonlyTransactionManager(web3j, address.pblcKy)
        is Account.PublicAccount -> ClientTransactionManager(web3j, address.pblcKy)
        is Account.PrivateAccount -> FastRawTransactionManager(web3j, credentials)
        // wallet is not unlocked yet
        is Account.WalletFileAccount -> ClientTransactionManager(web3j, address.pblcKy)
    }
}

fun Observable<Either<String, Pair<Account,String>>>.unlockAccountPair(): Observable<Either<String, Account>> {
    return scan {
        pAcc, pNew ->
        pNew bind {
            (newAcc, newPass) ->
            if (pAcc is Either.Right
                    && pAcc.r.first.address == newAcc.address
                    && pAcc.r.first is Account.PrivateAccount) {
                pAcc
            } else {
                Either.ret<String, Pair<Account, String>>(Pair(newAcc.try2Unlock(newPass), newPass))
            }
        }
    }.fmap { it.first.try2Unlock(it.second) }
}

private fun Double.format(digits: Int): String {
    return java.lang.String.format("%.${digits}f", this)
            .replace(",", ".")
            .replace(".00", "")
}

private fun BigInteger.toEtherPart(unit: Convert.Unit): Triple<String, Boolean, Convert.Unit> {
    val intPart = Convert.fromWei(toBigDecimal(), unit).toInt()
    val intTotalPart = Convert.fromWei(toBigDecimal().multiply(BigDecimal.valueOf(1000L)), unit).toInt()
    return Triple((intTotalPart / 1000.0).format(2), intPart > 0, unit)
}

fun BigInteger.toEtherPart(): String {
    val units = listOf(
            Convert.Unit.ETHER, Convert.Unit.FINNEY, Convert.Unit.SZABO,
            Convert.Unit.GWEI, Convert.Unit.MWEI, Convert.Unit.KWEI, Convert.Unit.WEI)

    val unit = units.firstOrNull { this.toEtherPart(it).second } ?: Convert.Unit.WEI
    return "${toEtherPart(unit).first} ${toEtherPart(unit).third.name}"
}

fun BigInteger.toEtherPercent(): Int {
    return toBigDecimal()
            .multiply(BigDecimal.valueOf(100))
            .divide(Convert.toWei("1", Convert.Unit.ETHER))
            .toBigInteger()
            .toInt()
}