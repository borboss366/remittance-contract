package cryqz.trader.accounts

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import cryqz.trader.web3.EtherAddress
import cryqz.util.Either
import cryqz.util.unlock
import org.web3j.crypto.Credentials
import org.web3j.crypto.WalletFile
import org.web3j.crypto.WalletUtils
import java.io.File

/*
Implementation is from https://stackoverflow.com/questions/26444145/extend-data-class-in-kotlin
 */
sealed class Account(open val address: EtherAddress = EtherAddress.createZeroAddress(),
                     open val name: String = "") {

    open fun try2Unlock(password: String): Account = this
    abstract val type: String

    data class ReadOnly(override val name: String = "") : Account(EtherAddress.createZeroAddress()) {
        override fun toString(): String = "Readonly"
        override val type: String
            get() = "Readonly"
    }

    class PublicAccount(override val address: EtherAddress,
                        override val name: String = ""): Account() {
        override fun toString(): String = "Public $address"
        override val type: String
            get() = "Public"
    }

    class PrivateAccount(val credentials: Credentials,
                         override val name: String = ""): Account(EtherAddress.createPrefixed(credentials.address)) {
        override fun toString(): String = "Private $address"
        override val type: String
            get() = "Private"
    }

    class WalletFileAccount(val walletFile: WalletFile,
                            override val name: String = ""): Account(EtherAddress.createPrefixed(walletFile.address)) {
        override fun toString(): String = "Encrypted $address"
        override val type: String
            get() = "Locked"

        override fun try2Unlock(password: String): Account {
            val unlock = walletFile.unlock(password)
            return when (unlock) {
                is Either.Right<String, Credentials> -> PrivateAccount(unlock.r)
                is Either.Left<String, Credentials> -> this
            }
        }
    }

    companion object {
        private val objectMapper = ObjectMapper()

        init {
            objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        }

        fun loadCrypto(file: File): WalletFile {
            return objectMapper.readValue<WalletFile>(file, WalletFile::class.java)
        }

        fun loadCryptoE(file: File): Either<String, WalletFile> {
            return try {
                Either.ret(loadCrypto(file))
            } catch (e: Exception) {
                Either.left(e.message ?: "failed loading crypto")
            }
        }

        fun loadCryptoEA(file: File): Either<String, Account> {
            return loadCryptoE(file).bind { Either.ret<String, Account>(WalletFileAccount(it)) }
        }

        fun loadCredentials(password: String, file: File): Credentials {
            return WalletUtils.loadCredentials(password, file)
        }

        fun loadCredentialsE(password: String, file: File): Either<String, Credentials> {
            return try {
                Either.ret(loadCredentials(password, file))
            } catch (e: Exception) {
                Either.left(e.message ?: "failed loading credentials")
            }
        }
    }
}