package cryqz.util

import org.web3j.crypto.Credentials
import org.web3j.crypto.Wallet
import org.web3j.crypto.WalletFile
import rx.Observable


fun <L: Any, R: Any> Observable<Either<L, R>>.filterPred(phi: (R) -> Boolean): Observable<Either<L, R>> {
    return flatMap {
        if (it is Either.Right<L, R>) Observable.just(it.r).filter(phi)
        else Observable.empty()
    }.map { Either.ret<L, R>(it) }
}


fun <L: Any, R: Any> Observable<Either<L, R>>.filterRight(): Observable<R> {
    return flatMap {
        if (it is Either.Right<L, R>) Observable.just(it.r)
        else Observable.empty()
    }
}

fun <L: Any, R: Any> Observable<Either<L, R>>.filterLeft(): Observable<L> {
    return flatMap {
        if (it is Either.Left<L, R>) Observable.just(it.l)
        else Observable.empty()
    }
}

fun <L: Any, R: Any, Rp: Any> Observable<Either<L, R>>.fmap(f: (R) -> Rp): Observable<Either<L, Rp>> {
    return map { it bind { v -> Either.ret<L, Rp>(f(v)) }  }
}

fun <L: Any, R: Any, Rp: Any> Observable<Either<L, R>>.bindEither(f: (R) -> (Either<L, Rp>)): Observable<Either<L, Rp>> {
    return map { it bind f }
}

//fun <R: Any, Rp: Any> Observable<Either<String, R>>.flatBindSingleEither(
//        f: (R) -> (Observable<Either<String, Rp>>)): Observable<Either<String, Rp>> {
//    return flatMap {
//        when(it) {
//            is Either.Right -> try {
//                f.invoke(it.r).single()
//            } catch(e: Exception) {
//                Observable.just(Either.Left<String, Rp>("Error flat bind single either $e.message"))
//            }
//            is Either.Left -> Observable.just(Either.Left(it.l))
//        }
//    }
//}

fun <R: Any, Rp: Any> Observable<Either<String, R>>.flatBindSingle(f: (R) -> (Observable<Rp>)): Observable<Either<String, Rp>> {
    return flatMap {
        when(it) {
            is Either.Right -> f.invoke(it.r).single()
                    .map { Either.ret<String, Rp>(it) }
                    .onErrorReturn { Either.left("Flat bind single fail $it.message") }
            is Either.Left -> Observable.just(Either.left(it.l))
        }
    }
}

//fun <R: Any, Rp: Any> Observable<Either<String, R>>.switchBindEither(
//        f: (R) -> (Observable<Either<String, Rp>>)): Observable<Either<String, Rp>> {
//    return switchMap {
//        when(it) {
//            is Either.Right -> f.invoke(it.r)
//            is Either.Left -> Observable.just(Either.Left<String, Rp>(it.l))
//        }
//    }.onErrorReturn { Either.Left("Error switch bind either ${it.message}") }
//}
//
//fun <R: Any, Rp: Any> Observable<Either<String, R>>.switchBind(
//        f: (R) -> (Observable<Rp>)): Observable<Either<String, Rp>> {
//    return switchMap {
//        when(it) {
//            is Either.Right -> f.invoke(it.r).map { Either.Right<String, Rp>(it) }
//            is Either.Left -> Observable.just(Either.Left<String, Rp>(it.l))
//        }
//    }.onErrorReturn { Either.Left("Error flat bind ${it.message}") }
//}

fun WalletFile.unlock(password: String): Either<String, Credentials> {
    return try {
        Either.ret(Credentials.create(Wallet.decrypt(password, this)))
    } catch(e: Exception) {
        Either.left("Failed unlocking ${e.message}")
    }
}

fun <L: Any, R: Any>Observable<Either<L, R>>.testRight() {
    filter { it.isRight() }.single()
}

fun <L: Any, R: Any>Observable<Either<L, R>>.testLeft() {
    filter { it.isLeft() }.single()
}

fun <L: Any, R: Any> Either<L, R>.o(): Observable<Either<L, R>> {
    return Observable.just(this)
}


fun <L: Any, R1: Any, R2: Any> Either<L, R1>.bind2Pair(e2: Either<L, R2>): Either<L, Pair<R1, R2>> {
    return bind { it -> e2 bind { it2 -> Either.ret<L, Pair<R1, R2>>(Pair(it, it2)) } }
}

fun <L: Any, R1: Any, R2: Any, RR: Any> Observable<Either<L, R1>>
        .bindWithE(o2: Observable<Either<L, R2>>, phi: (r1: R1, r: R2) -> RR): Observable<Either<L, RR>> {
    return Observable.combineLatest(this, o2) {
        e1, e2 -> e1.bind { e1v -> e2.bind { e2v -> Either.ret<L, RR>(phi(e1v, e2v)) } }
    }
}

fun <L: Any, R1: Any, R2: Any> Observable<Either<L, R1>>
        .zipWithPair(o2: Observable<Either<L, R2>>): Observable<Either<L, Pair<R1, R2>>> {
    return bindWithE(o2) { r1, r2 -> Pair(r1, r2)}
}


fun <L: Any, R: Any> Either<L, R>.makeStringFromWhatIsInside():String {
    return when(this) {
        is Either.Right -> "$r"
        is Either.Left -> "$l"
    }
}

fun <L: Any, R: Any> Either<L, R>.initialized(): String {
    return when(this) {
        is Either.Right -> "Initialized"
        is Either.Left -> "$l"
    }
}

fun <L: Any, R: Any> Either<L, R>.toDefault(def: R): R {
    return when(this) {
        is Either.Right -> r
        is Either.Left -> def
    }
}

fun <R: Any> R?.fromNull(message: String = "Null"): Either<String, R> =
        if (this != null) Either.ret(this) else Either.left(message)