package cryqz.util

sealed class Either<L, R> {
    class Left<L, R>(val l: L) : Either<L, R>() {
        override fun toString(): String = "Left $l"
    }

    class Right<L, R>(val r: R) : Either<L, R>() {
        override fun toString(): String = "Right $r"
    }

    fun isLeft(): Boolean = this is Either.Left
    fun isRight(): Boolean = this is Either.Right

    infix fun <Rp> bind(f: (R) -> (Either<L, Rp>)): Either<L, Rp> {
        return when (this) {
            is Either.Left<L, R> -> Left(this.l)
            is Either.Right<L, R> -> f(this.r)
        }
    }

    infix fun <Rp> seq(e: Either<L, Rp>): Either<L, Rp> {
        return when(this) {
            is Either.Left<L, R> -> Left(this.l)
            is Either.Right<L, R> -> e
        }
    }

    companion object {
        fun <L, R> left(l: L): Either<L, R> = Either.Left(l)
        fun <L, R> ret(a: R): Either<L, R> = Either.Right(a)
        fun <L, R> fail(msg: String): Either<L, R> = throw Exception(msg)
    }
}