package sandbox

import cryqz.trader.accounts.Account
import cryqz.util.Either
import cryqz.util.filterPred
import cryqz.util.filterRight
import io.kotlintest.specs.StringSpec
import rx.observers.TestSubscriber
import rx.subjects.BehaviorSubject
import java.io.File

class TestAcc: StringSpec() {
    init {
        val walletFile = File("UTC--2017-12-16T18-09-08.321240712Z--f0fcbccd0bdda74b8ea38c7f119e64eb3c199cb5")
        val wallet = Account.WalletFileAccount(Account.loadCrypto(walletFile))

        "test distinct" {
            val accountSubject = BehaviorSubject.create<Either<String, Account>>(Either.left("Account not initialized"))
            val co = accountSubject.asObservable().publish()
            val testS = TestSubscriber<Account>()

            accountSubject.onNext(Either.ret(wallet))
            accountSubject.onNext(Either.ret(wallet))
            co.filterRight().subscribe(testS)
            co.connect()

            assert(testS.valueCount == 1)

        }
        "test the observables" {
            val accountSubject = BehaviorSubject.create<Either<String, Account>>(Either.left("Account not initialized"))
            val passwordSubject = BehaviorSubject.create<String>("")

            val accountO = accountSubject.asObservable().distinct()
            val passwordO = passwordSubject.asObservable()
            val unlockedO = passwordO.withLatestFrom(accountO) {
                pe, ae -> ae bind { a -> Either.ret<String, Account>(a.try2Unlock(pe)) }
            }.filterPred { it is Account.PrivateAccount }

            val resultAccountO = accountO.mergeWith(unlockedO).filterRight()
            val co = resultAccountO.publish()
            val testS = TestSubscriber<Account>()
            co.subscribe(testS)

            co.connect()
            accountSubject.onNext(Either.ret(wallet))
            passwordSubject.onNext("Zalupa")
            testS.assertValueCount(1)

            passwordSubject.onNext("Q1w2e3r4t5")
            testS.assertValueCount(2)
            assert(testS.onNextEvents.last() is Account.PrivateAccount)

            passwordSubject.onNext("Q1w2e3r4t")
            testS.assertValueCount(2)
            assert(testS.onNextEvents.last() is Account.PrivateAccount)
        }
    }
}