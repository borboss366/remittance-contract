package sandbox

import io.kotlintest.specs.StringSpec
import rx.Observable
import rx.lang.kotlin.subscribeBy
import rx.lang.kotlin.toObservable
import rx.subjects.BehaviorSubject
import rx.subjects.ReplaySubject

class ScanTest: StringSpec() {
    init {
        "scantest" {
            listOf(2, 3, 4)
                    .toObservable()
                    .scan { i1, i2 ->
                        println("In $i1 $i2")
                        i1 + i2  }
                    .subscribeBy(onNext = ::println)
        }

        "scantest2" {
            val o1 = listOf(1, 2, 3).toObservable()
            val o2 = listOf(4, 5, 6).toObservable()

            Observable.combineLatest(o1, o2) {
                i1, i2 -> Pair(i1, i2)
            }.subscribeBy(onNext = ::println)
        }
        "scantest3" {
            val o1 = listOf(1, 2, 3).toObservable()
            val o2 = listOf("4", "5", "6").toObservable()

            Observable.zip(o1, o2) {
                i1, i2 -> Pair(i1, i2)
            }.subscribeBy(onNext = ::println)
        }
        "scantest4" {
            val o1 = listOf(1, 2, 3).toObservable()
            o1.startWith(0).buffer(2, 1).subscribeBy(onNext = ::println)
        }

        "scantest5" {
            val bh1 = BehaviorSubject.create(0)
            val bh2 = BehaviorSubject.create(10)

            val latest = Observable.combineLatest(bh1, bh2) {
                i1, i2 -> Pair(i1, i2)
            }

            latest.subscribeBy(onNext = ::println)

            bh1.onNext(2)
            bh1.onNext(3)
            bh2.onNext(20)
            bh2.onNext(30)

            latest.map { "2 $it" }.subscribeBy(onNext = ::println)
        }

        "replay test" {
            val replaySubj = ReplaySubject.createWithSize<String>(2)

            val o = replaySubj.asObservable().startWith("Zalupa")

            replaySubj.onNext("x1")
            replaySubj.onNext("x2")
            replaySubj.onNext("x3")
            replaySubj.onNext("x4")


            o.subscribeBy(onNext = ::println)
            o.subscribeBy(onNext = ::println)
        }
    }
}