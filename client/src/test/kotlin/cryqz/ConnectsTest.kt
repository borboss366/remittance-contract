package cryqz

import cryqz.trader.web3.NetworkId
import cryqz.trader.models.web3.Web3Model
import cryqz.trader.models.web3.Web3NetworkId
import cryqz.trader.models.web3.buildWeb3j
import cryqz.trader.web3.ConnectInfo
import cryqz.util.Either
import io.kotlintest.specs.StringSpec
import org.web3j.protocol.Web3j
import rx.observers.TestSubscriber
import java.math.BigInteger

class ConnectsTest : StringSpec() {
    init {
        val localCI = ConnectInfo.buildLocalhostPlain()
        val wrongCI = ConnectInfo.buildFromAddress("http://127.0.0.1", 8546)


        "the correct connects info should be emitted" {
            val model = Web3Model.Web3SingleModel()
            model.connect(localCI)
            model.connectInfoO().testSingleRightValue(localCI)
            model.connectInfoO().testLeftEmpty()
        }

        "the correct web3 web3Model should be emitted" {
            val model = Web3Model.Web3SingleModel()
            model.connect(localCI)
            model.connectInfoO().buildWeb3j(model.pollingIntervalO(), model.serviceO()).testLeftEmpty()
            model.connectInfoO().buildWeb3j(model.pollingIntervalO(), model.serviceO()).testSingleRightValue()
        }

        "the correct web3 network" {
            val model = Web3Model.Web3SingleModel()
            model.connect(localCI)
            model.networkVersionO().testLeftEmpty()
            model.networkVersionO().testSingleRightValue(NetworkId.createFrom("666"))
        }

        "the correct web3 network" {
            val model = Web3Model.Web3SingleModel()
            model.connect(localCI)
            model.networkGasPriceO().testLeftEmpty()
            model.networkGasPriceO().testSingleRightValue()
        }

        "the custom gas price test" {
            val model = Web3Model.Web3SingleModel()
            model.connect(localCI)
            model.setCustomGasPrice(BigInteger.ONE)
            model.customGasPrice0().testSingleValue(BigInteger.ONE)
        }

        "the no price if no connects test" {
            val model = Web3Model.Web3SingleModel()
            model.setCustomGasPrice(BigInteger.ONE)
            model.gasPriceO().testLeftValueCount(2)
        }

        "the no price if no connects test" {
            val model = Web3Model.Web3SingleModel()
            model.connect(wrongCI)
            model.setCustomGasPrice(BigInteger.ONE)
            model.gasPriceO().doOnNext { println(it) }.testLeftValueCount(2)
        }


        "the number of web3j" {
            val model = Web3Model.Web3SingleModel()
            val w3p = model.web3jO().publish()
            val testS = TestSubscriber<Either<String, Web3j>>()
            w3p.subscribe(testS)

            w3p.connect()
            model.connect(localCI)
            model.connect(wrongCI)
            model.connect(localCI)
            testS.assertValueCount(4)
        }

        "the number of web3 vesions" {
            val model = Web3Model.Web3SingleModel()
            val w3p = model.networkVersionO().publish()
            val testS = TestSubscriber<Either<String, NetworkId>>()
            w3p.doOnNext(::println).subscribe(testS)

            w3p.connect()
            model.connect(localCI)
            model.connect(wrongCI)
            model.connect(localCI)
            testS.assertValueCount(4)
        }
    }
}