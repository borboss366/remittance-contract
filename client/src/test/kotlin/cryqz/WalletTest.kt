package cryqz

import cryqz.trader.accounts.Account
import cryqz.util.bindEither
import cryqz.util.testLeft
import cryqz.util.unlock
import io.kotlintest.matchers.shouldThrow
import io.kotlintest.specs.StringSpec
import org.web3j.crypto.CipherException
import org.web3j.crypto.Credentials
import org.web3j.crypto.Wallet
import rx.Observable
import java.io.File

class WalletTest : StringSpec() {
    init {
        val walletFile = File("UTC--2017-12-16T18-09-08.321240712Z--f0fcbccd0bdda74b8ea38c7f119e64eb3c199cb5")
        val wrongWalletFile = File("UTC--2017-12-16T18-09-08.321240712Z--")

        "should load wallet" {
            val wallet = Account.loadCrypto(walletFile)

            // opening with wrong password
            shouldThrow<CipherException> {
                Credentials.create(Wallet.decrypt("q1w2e3r4t5", wallet))
            }

            Credentials.create(Wallet.decrypt("Q1w2e3r4t5", wallet))
        }

        "should load wallet either" {
            assert(Account.loadCryptoE(walletFile).isRight())
        }

        "should load credentials" {
            Account.loadCredentials("Q1w2e3r4t5", walletFile)

            // opening with wrong password
            shouldThrow<CipherException> {
                Account.loadCredentials("q1w2e3r4t5", walletFile)
            }
        }

        "should fail to load crypto e" {
            assert(Account.loadCryptoE(wrongWalletFile).isLeft())
            Observable.just(Account.loadCryptoE(wrongWalletFile))
                    .bindEither { it.unlock("Q1w2e3r4t5") }
                    .testLeft()
        }

        "should load credentials either" {
            assert(Account.loadCredentialsE("Q1w2e3r4t5", walletFile).isRight())
            assert(Account.loadCredentialsE("q1w2e3r4t5", walletFile).isLeft())
        }

        "should fail to load creadentials e" {
            assert(Account.loadCredentialsE("Q1w2e3r4t5", wrongWalletFile).isLeft())
            assert(Account.loadCredentialsE("q1w2e3r4t5", walletFile).isLeft())
        }
    }
}