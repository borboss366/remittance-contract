package cryqz

import cryqz.util.Either
import cryqz.util.filterLeft
import cryqz.util.filterRight
import rx.Observable
import rx.observers.TestSubscriber

fun <L: Any, R: Any> Observable<Either<L, R>>.testLeftEmpty() {
    val testLeft = TestSubscriber<L>()
    filterLeft().subscribe(testLeft)
    testLeft.assertValueCount(0)
    testLeft.unsubscribe()
}


fun <L: Any, R: Any> Observable<Either<L, R>>.testLeftValueCount(count: Int) {
    val testLeft = TestSubscriber<L>()
    filterLeft().subscribe(testLeft)
    testLeft.assertValueCount(count)
    testLeft.unsubscribe()
}

fun <L: Any, R: Any> Observable<Either<L, R>>.testSingleLeftValue() = testLeftValueCount(1)

fun <L: Any, R: Any> Observable<Either<L, R>>.testSingleRightValue(ev: R) {
    val testRight = TestSubscriber<R>()
    filterRight().subscribe(testRight)
    testRight.assertValueCount(1)
    testRight.assertValue(ev)
    testRight.unsubscribe()
}

fun <L: Any, R: Any> Observable<Either<L, R>>.testSingleRightPredicate(f: (R) -> Boolean) {
    val testRight = TestSubscriber<Boolean>()
    filterRight().map { f(it) }.subscribe(testRight)
    testRight.assertValueCount(1)
    testRight.assertValue(true)
    testRight.unsubscribe()
}

fun <L: Any, R: Any> Observable<Either<L, R>>.testLastRightPredicate(f: (R) -> Boolean) {
    val testRight = TestSubscriber<Boolean>()
    filterRight().map { f(it) }.subscribe(testRight)
    val onNextEvents = testRight.onNextEvents
    assert(onNextEvents.size > 0)
    assert(onNextEvents.last())
    testRight.unsubscribe()
}


fun <L: Any, R: Any> Observable<Either<L, R>>.testSingleRightValue() {
    val testRight = TestSubscriber<R>()
    filterRight().subscribe(testRight)
    testRight.assertValueCount(1)
    testRight.unsubscribe()
}

fun <R: Any> Observable<R>.testSingleValue(ev: R) {
    val testRight = TestSubscriber<R>()
    subscribe(testRight)
    testRight.assertValueCount(1)
    testRight.assertValue(ev)
    testRight.unsubscribe()
}

