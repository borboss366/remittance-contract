package cryqz

import cryqz.trader.accounts.Account
import cryqz.trader.models.accounts.AccountsModelStub
import cryqz.trader.models.web3.Web3Model
import cryqz.trader.web3.EtherAddress
import cryqz.util.Either
import cryqz.util.filterRight
import cryqz.util.fmap
import cryqz.util.o
import io.kotlintest.specs.StringSpec
import org.web3j.crypto.Credentials
import rx.lang.kotlin.subscribeBy
import rx.observers.TestSubscriber
import java.io.File

class AccountsTest: StringSpec() {
    init {
        val walletFile = File("UTC--2017-12-16T18-09-08.321240712Z--f0fcbccd0bdda74b8ea38c7f119e64eb3c199cb5")

        "test account" {
            val model = Web3Model.Web3SingleModel()
            model.unlockedAccountO().testLeftValueCount(1)
        }

        "readonly equal" {
            val readOnly1: Account = Account.ReadOnly()
            val readOnly2: Account = Account.ReadOnly()
            assert(readOnly1 == readOnly2)

            val walletAdd: Account = Account.WalletFileAccount(Account.loadCrypto(walletFile))
            assert(walletAdd != readOnly1)
            val unlocked = walletAdd.try2Unlock("Q1w2e3r4t5")
            assert(walletAdd != unlocked)

            val unlocked2 = unlocked.try2Unlock("Q1w2e3r4t5")
            assert(unlocked == unlocked2)
        }

        "test read only account" {
            val model = Web3Model.Web3SingleModel()
            model.setAccount(Account.ReadOnly())
            model.unlockedAccountO().fmap { it.address }.testSingleRightValue(Account.ReadOnly().address)
        }

        "test client account" {
            val model = Web3Model.Web3SingleModel()
            val cred = Account.loadCredentials("Q1w2e3r4t5", walletFile)
            model.setAccount(Account.PublicAccount(EtherAddress.createPrefixed(cred.address)))
            model.unlockedAccountO().fmap { it.address.pblcKy }.testSingleRightValue(cred.address)
        }

        "test wallet account" {
            val model = Web3Model.Web3SingleModel()
            val wallet = Account.loadCrypto(walletFile)
            model.setAccount(Account.PublicAccount(EtherAddress.createPrefixed(wallet.address)))
            model.unlockedAccountO().fmap { it.address }.testSingleRightValue(EtherAddress.createPrefixed(wallet.address))
        }

        "test private account" {
            val model = Web3Model.Web3SingleModel()
            val cred = Account.loadCredentials("Q1w2e3r4t5", walletFile)
            Either.ret<String, Credentials>(cred).o()
                    .fmap { Account.PrivateAccount(it) }
                    .filterRight()
                    .subscribeBy(onNext = model::setAccount)
            model.unlockedAccountO().fmap { it.address.pblcKy }.testSingleRightValue(cred.address)
        }

        "test wallet unlock account" {
            val model = Web3Model.Web3SingleModel()
            val wallet = Account.loadCrypto(walletFile)
            val walletAccount = Account.WalletFileAccount(wallet)
            model.setAccount(walletAccount)

            model.unlockedAccountO().fmap { it.address }.testSingleRightValue(walletAccount.address)
            model.unlockedAccountO().fmap { it.address }.testSingleRightValue(walletAccount.address)

            model.unlockedAccountO().testSingleRightPredicate { it is Account.WalletFileAccount }
            model.unlockAccount("nifiga")
            model.unlockedAccountO().testLastRightPredicate { it is Account.WalletFileAccount }

            model.unlockAccount("Q1w2e3r4t5")
            model.unlockedAccountO().testLastRightPredicate { it is Account.PrivateAccount }
        }

        "test unlocked don't change after wrong pass" {
            val model = Web3Model.Web3SingleModel()
            val wallet = Account.loadCrypto(walletFile)
            val walletAccount = Account.WalletFileAccount(wallet)
            model.setAccount(walletAccount)

            val ci = model.unlockedAccountO().publish()
            val testRight = TestSubscriber<Boolean>()
            ci.filterRight().map { it is Account.PrivateAccount }.subscribe(testRight)
            ci.connect()
            model.unlockAccount("Q1w2e3r4t5")
            model.unlockAccount("nifiga")
            testRight.assertValues(false, true)
        }

        "amount of accounts should be the same" {
            val file = File("accounts")
            val size = file.listFiles().size
            assert(size > 0)
            val model = AccountsModelStub(file)
            model.controlAccounts().subscribeBy(onNext = { assert(size == it.size) })
        }

        "test wallet unlock account" {
            val model = Web3Model.Web3SingleModel()
            val wallet = Account.loadCrypto(walletFile)
            val walletAccount = Account.WalletFileAccount(wallet)
            model.setAccount(walletAccount)

            model.unlockedAccountO().subscribeBy(onNext = { println("Unlocked $it") })

            model.unlockAccount("nifiga")
            model.unlockAccount("Q1w2e3r4t5")
            model.unlockAccount("nifiga")
        }
    }
}