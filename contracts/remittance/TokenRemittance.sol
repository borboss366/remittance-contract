pragma solidity 0.4.23;

import "./Token.sol";

contract TokenRemittance {
    address public admin;
    mapping (address => bool) allowedTokens;

    constructor(address _admin) public {
        admin = _admin;
    }

    function allowToken(address token) public onlyAdmin {
        allowedTokens[token] = true;
    }

    enum ContractStatus { 
        Holder1Deposited, // the first holder deposited to the contract
        BothDeposited, // both of the holders have deposited to the contract
        Proposed, // the receiver was adviced
        Decided // the receiver was decided
    }

    struct TwoHoldersContract {
        ContractStatus status;

        address holder1; // the address of the first holder of this contract
        address holder2; // the address of the second holder of this contract
        uint contractStarted;       // the block, when both of the holders have deposited, 
                                    // this is the time, when the contract becomes active

        uint contractLength; // the contract time before the expiration

        // the address of the token, that is used for this operation
        address token;
        uint amountHolder1; // the amount of tokens, that holder1 holds
        uint amountHolder2; // the amount of tokens, thatr holder2 holds
        
        
        address receiver; // the address, that should receive the desired money
        address proposer; // the address that proposes
    }

    mapping (bytes32 => TwoHoldersContract) contracts;
    event ContractCreated(bytes32 contractId, address token, address holder1, address holder2, uint amount1, uint amount2, uint length);
    event SecondDeposited(bytes32 contractId, address holder2, uint amount2);
    event Proposed(bytes32 contractId, address holder, address receiver);
    event Decided(bytes32 contractId, address holder, address receiver);
    event Rejected(bytes32 contractId, address holder, address receiver);
    event HolderWithdrawn(bytes32 contractId, address holden, address token, uint amount);
    event ReceiverWithdrawn(bytes32 contractId, address receiver, address token, uint amount);

    function createContract(
        address token, 
        address holder2, 
        uint amount1, 
        uint amount2, 
        uint length, 
        uint nonce) public returns(bytes32) {

        // the hash, which has all of the parameters of the contract
        bytes32 contractHash = keccak256(token, msg.sender, holder2, amount1, amount2, length, nonce);
        // the contract should not exist
        require(!exists(contractHash));
        // the token should be allowed by remittance admin
        require(allowedTokens[token]);
        require(amount1 > 0);
        require(amount2 > 0);
        require(length > 0);

        // it should be possible to transfer the token to this contract
        if (!Token(token).transferFrom(msg.sender, this, amount1)) revert();

        contracts[contractHash].status = ContractStatus.Holder1Deposited;
        // let the start of the contract withh be the current block
        contracts[contractHash].contractStarted = block.number;
        contracts[contractHash].token = token;
        contracts[contractHash].holder1 = msg.sender;
        contracts[contractHash].holder2 = holder2;
        contracts[contractHash].amountHolder1 = amount1;
        contracts[contractHash].amountHolder2 = amount2;
        contracts[contractHash].contractLength = length;
    
        emit ContractCreated(contractHash, token, msg.sender, holder2, amount1, amount2, length);
        return contractHash;
    } 

    function exists(bytes32 contractHash) public view returns(bool) {
        return contracts[contractHash].holder1 == address(0);
    }

    function expired(bytes32 contractHash) public view returns(bool) {
        if (!exists(contractHash)) return false;
        return contracts[contractHash].contractStarted + contracts[contractHash].contractLength < block.number;
    }

    function holderRequest(bytes32 contractHash) public view returns(bool) {
        return msg.sender == contracts[contractHash].holder1 
            || msg.sender == contracts[contractHash].holder2;
    }

    function depositSecondHolder(bytes32 contractHash, uint amount) public {
        require(exists(contractHash));
        require(contracts[contractHash].status == ContractStatus.Holder1Deposited);
        require(!expired(contractHash));
        require(msg.sender == contracts[contractHash].holder2);
        require(amount == contracts[contractHash].amountHolder2);
        if (!Token(contracts[contractHash].token).transferFrom(msg.sender, this, amount)) revert();

        contracts[contractHash].status = ContractStatus.BothDeposited;
        emit SecondDeposited(contractHash, msg.sender, amount);
    }

    function proposeReceiver(bytes32 contractHash, address receiver) public {
        require(exists(contractHash));
        require(contracts[contractHash].status == ContractStatus.BothDeposited);
        require(!expired(contractHash));
        require(holderRequest(contractHash));
        contracts[contractHash].receiver = receiver;
        contracts[contractHash].proposer = msg.sender;
        contracts[contractHash].status = ContractStatus.Proposed;
        emit Proposed(contractHash, msg.sender, receiver);
    }

    function acceptReceiver(bytes32 contractHash) public {
        require(exists(contractHash));
        require(contracts[contractHash].status == ContractStatus.Proposed);
        require(!expired(contractHash));
        require(holderRequest(contractHash));
        require(msg.sender != contracts[contractHash].proposer);
        contracts[contractHash].status = ContractStatus.Decided;
        emit Decided(contractHash, msg.sender, contracts[contractHash].receiver);
    }

    function rejectReceiver(bytes32 contractHash) public {
        require(exists(contractHash));
        require(contracts[contractHash].status == ContractStatus.Proposed);
        require(!expired(contractHash));
        require(holderRequest(contractHash));
        require(msg.sender != contracts[contractHash].proposer);
        contracts[contractHash].status = ContractStatus.BothDeposited;
        emit Rejected(contractHash, msg.sender, contracts[contractHash].receiver);
    }
    
    function returnToReceiver(bytes32 contractHash) public {
        require(exists(contractHash));
        require(contracts[contractHash].status == ContractStatus.Decided);
        require(msg.sender == contracts[contractHash].receiver);
        uint toReturn = contracts[contractHash].amountHolder1 + contracts[contractHash].amountHolder2;
        contracts[contractHash].amountHolder1 = 0;
        contracts[contractHash].amountHolder2 = 0;
        if (!Token(contracts[contractHash].token).transferFrom(this, msg.sender, toReturn)) revert();
        emit ReceiverWithdrawn(contractHash, contracts[contractHash].receiver, contracts[contractHash].token, toReturn);
    }


    // in case the contract was failure, return the tokens
    function returnTokens(bytes32 contractHash) public {
        require(exists(contractHash));
        require(expired(contractHash));
        require(holderRequest(contractHash));

        uint toReturn = 0;
        // only the first deposited
        if (contracts[contractHash].status == ContractStatus.Holder1Deposited) {
            require(msg.sender == contracts[contractHash].holder1);
            require(contracts[contractHash].amountHolder1 > 0);
            toReturn = contracts[contractHash].amountHolder1;

            // make the amount1 zero
            contracts[contractHash].amountHolder1 = 0;
            // transfer the money back
        } else if (contracts[contractHash].status != ContractStatus.Decided) {
            if (msg.sender == contracts[contractHash].holder1) {
                require(contracts[contractHash].amountHolder1 > 0);
                toReturn = contracts[contractHash].amountHolder1;
                contracts[contractHash].amountHolder1 = 0;
            } else if (msg.sender == contracts[contractHash].holder2) {
                require(contracts[contractHash].amountHolder2 > 0);
                toReturn = contracts[contractHash].amountHolder2;
                contracts[contractHash].amountHolder2 = 0;
            }
        } 

        if (!Token(contracts[contractHash].token).transferFrom(this, msg.sender, toReturn)) revert();
        emit ReceiverWithdrawn(contractHash, msg.sender, contracts[contractHash].token, toReturn);
    }

    modifier onlyAdmin() {
        require(msg.sender == admin);
        _;
    }
}