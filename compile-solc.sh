solcjs contracts/remittance/TokenRemittance.sol contracts/remittance/Token.sol --bin --abi --optimize --optimize-runs 200 --overwrite -o build/remittanceweb3j
web3j solidity generate build/remittanceweb3j/TokenRemittance.bin build/remittanceweb3j/TokenRemittance.abi -o client/src/main/java -p remittance.contract
